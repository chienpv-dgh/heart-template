import Head from 'next/head'
import Script from 'next/script'

export default function Home() {
  return (
    <div className=''>
      <Script src="../library.js"></Script>

      <Head>
        <title>Iu Phi Béo Mập</title>
        <meta name="description" content="Phi mập ú" />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:site_name" content="Béo Ú"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content='Iu Phi Béo Mập' />
        <meta property="og:description" content='Phi mập ú' />
        <meta property="og:image" content='image.jpg' />
      </Head>

      <div className="box">
        <canvas id="pinkboard"></canvas>
      </div>

      <div className="center-text"
        style={{
          backgroundColor: 'rgb(0, 0, 0)',
          width: '100%',
          color: 'rgb(225, 12, 168)',
          height: '100%',
          fontSize: '31px',
          fontStyle: 'italic',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: '5px',
          textAlign: 'center',
        }}
      >Phi Béo Mập</div>
    </div>
  )
}
